/* While this template provides a good starting point for using Wear Compose, you can always
 * take a look at https://github.com/android/wear-os-samples/tree/main/ComposeStarter and
 * https://github.com/android/wear-os-samples/tree/main/ComposeAdvanced to find the most up to date
 * changes to the libraries and their usages.
 */

package com.battrat.haiku1.presentation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.IOException
import java.util.*
import com.battrat.haiku1.R
import com.battrat.haiku1.presentation.theme.Haiku1Theme

class MainActivity : AppCompatActivity() {

    private lateinit var responseText: TextView
    private lateinit var fetchButton: Button
    private lateinit var speechButton: Button
    private val client = OkHttpClient()
    private val SPEECH_REQUEST_CODE = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        responseText = findViewById(R.id.responseText)
        fetchButton = findViewById(R.id.fetchButton)
        speechButton = findViewById(R.id.speechButton)

        fetchButton.setOnClickListener {
            fetchResponse("Tell me something interesting about Wear OS.")
        }

        speechButton.setOnClickListener {
            displaySpeechRecognizer()
        }
    }

    private fun fetchResponse(userInput: String) {
        val json = JSONObject()
        json.put("model", "gpt-3.5-turbo")
        json.put("messages", createMessagesArray(userInput))

        val body = json.toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())
        val request = Request.Builder()
            .url("https://api.openai.com/v1/chat/completions")
            .addHeader("Authorization", "Bearer BuildConfig.MY_API_KEY")
            .post(body)
            .build()

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = client.newCall(request).execute()
                if (response.isSuccessful) {
                    val responseData = response.body?.string()
                    val jsonResponse = JSONObject(responseData)
                    val resultText = jsonResponse.getJSONArray("choices").getJSONObject(0).getJSONObject("message").getString("content")
                    withContext(Dispatchers.Main) {
                        responseText.text = resultText.trim()
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        responseText.text = "Error: ${response.message}"
                    }
                }
            } catch (e: IOException) {
                withContext(Dispatchers.Main) {
                    responseText.text = "Exception: ${e.message}"
                }
            }
        }
    }

    private fun createMessagesArray(userInput: String): String {
        val systemMessage = JSONObject()
        systemMessage.put("role", "system")
        systemMessage.put("content", "You are μChatGPT. You will respond to user messages using up to two very short five-word phrases. Use italics for words containing ideas to continue the conversation flow by allowing the user to repeat back an italicized word to expand the idea with a new response using the same rules as described. Also use emoji and carriage returns to aid in communication and for aesthetics. This is performance art, designed for persistent heads-up displays; so don't crowd the display, it could occlude someone's vision. Return the code 404 and a concise continuation prompt for unsafe conversation trees.")

        val userMessage = JSONObject()
        userMessage.put("role", "user")
        userMessage.put("content", userInput)

        val messagesArray = arrayOf(systemMessage, userMessage)
        return JSONObject().put("messages", messagesArray).toString()
    }

    private fun displaySpeechRecognizer() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
            putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        }
        startActivityForResult(intent, SPEECH_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val spokenText: String? = data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)?.let { results ->
                results[0]
            }
            spokenText?.let {
                fetchResponse(it)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}